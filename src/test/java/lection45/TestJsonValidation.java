//package lection45;
//
//import io.restassured.RestAssured;
//import io.restassured.http.ContentType;
//import io.restassured.path.json.JsonPath;
//import io.restassured.response.Response;
//import io.restassured.response.ResponseBody;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
//import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
//
//public class TestJsonValidation {
//    public static String BASE_URL = "https://petstore.swagger.io/v2";
//
//    @BeforeAll
//    public static void setUP(){
//        RestAssured.baseURI = BASE_URL;
//    }
//
//    @Test
//    public void testGetSchema(){
//        Response response = RestAssured.given()
//                .contentType(ContentType.JSON)
//                .when()
//                .get("/pet/159")
//                .then()
//                .body(matchesJsonSchemaInClasspath("schema.json"))
//                .and()
//                .extract()
//                .response();
//
//        ResponseBody responseBody = response.body();
//        System.out.println(responseBody.asString());
//
//        JsonPath jsonPath = response.jsonPath();
//        String dogName = jsonPath.get("name");
//
//        Assertions.assertEquals(200, response.statusCode());
//        Assertions.assertEquals("dogg", dogName, "Correct pet name received in the responce");
//    }
//}
