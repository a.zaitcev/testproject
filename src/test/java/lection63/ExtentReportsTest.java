package lection63;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogType;

import java.io.File;
import java.io.IOException;
import java.util.List;

public abstract class ExtentReportsTest {
    private static String reportName = "C:\\az data\\Projects\\IdeaProjects\\testproject\\src\\test\\resources\\default.html";
    private static ExtentReports reports = new ExtentReports();
    private static ExtentSparkReporter reporter;
    private static ExtentTest test;
    private static WebDriver driver;

    public static void setReport(String reportName) {
        if (reportName != null) {
            ExtentReportsTest.reportName = reportName;
        }
        reporter = new ExtentSparkReporter(reportName);
        reports.attachReporter(reporter);
    }

    public static void setDriver(WebDriver driver) {
        ExtentReportsTest.driver = driver;
    }

    public static ExtentReports report() {
        if (reports == null) {
            throw new RuntimeException("Report is not set");
        }
        return reports;
    }

    public static ExtentTest createTest(String testName, String testDescription) {
        test = ExtentReportsTest.report().createTest(testName, testDescription);
        return test;
    }

    public static ExtentTest test() {
        return test;
    }

    public static void shot(String name) {
        ExtentReportsTest.test().
                addScreenCaptureFromBase64String(
                        ((TakesScreenshot) driver).
                                getScreenshotAs(OutputType.BASE64),
                        name);
    }

    public static void shotToDisc(String name) throws IOException {
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File(name));
    }



    public static void getLog() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("console.log('HI')");
        List<String> performance = (List) js.executeScript(
                "return window.performance.getEntries().map((x) => {return JSON.stringify(x.toJSON())})"
        );
//        List performance = driver.manage().logs().get("performance").getAll();
        List browser = driver.manage().logs().get(LogType.BROWSER).getAll();
        List driverLog = driver.manage().logs().get(LogType.DRIVER).getAll();
        test.info(MarkupHelper.createCodeBlock(
//                performance.toString(),
                browser.toString(),
                driverLog.toString()
        ));
    }

    public static void flush() {
        ExtentReportsTest.report().flush();
    }
}

