package lection63;

import Helpers.ConfProperties;
import com.google.common.base.Function;
import io.qameta.allure.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.List;
import java.util.Set;

public class SeleniumTest {
    WebDriver driver;

    @BeforeEach
    public void setUP(){
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(20));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
        driver.manage().timeouts().setScriptTimeout(Duration.ofSeconds(20));
    }

    @Test
    public void firstBrowserTest1() throws InterruptedException {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));

        // do smth

        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(35));

    }

    @Test
    public void firstBrowserTest() throws InterruptedException {
        driver.get(ConfProperties.getProperty("startPage"));

        WebElement inputTextLogin = driver.findElement(By.cssSelector("#user-name"));
        inputTextLogin.sendKeys("standard_user");

        WebElement inputTextPassword = driver.findElement(By.cssSelector("#password"));
        inputTextPassword.sendKeys("secret_sauce");

        WebElement loginBth = driver.findElement(By.xpath("//*[@data-test='login-button']"));
        loginBth.click();

        Assertions.assertEquals(ConfProperties.getProperty("invertoryPage"), driver.getCurrentUrl().toString());

        // 1
        WebElement addGoodToCart = driver.findElement(By.xpath("//*[contains(text(), 'Sauce Labs Fleece Jacket')]/../../../div[2]/button"));
        addGoodToCart.click();

//         2
        WebElement addGood2ToCart = driver.findElement(By.xpath("//*[contains(text(), 'Sauce Labs Bike Light')]/../../../div[2]/button"));
        addGood2ToCart.click();

        Select sortingFilter = new Select(driver.findElement(By.xpath("//*[@data-test='product_sort_container']")));
        sortingFilter.selectByIndex(2);

        WebElement cart =  driver.findElement(By.cssSelector(".shopping_cart_badge"));
        String amountGoodsInCart = cart.getAttribute("innerText");
        String classAttrib = cart.getAttribute("class");

        Assertions.assertEquals(2, Integer.valueOf(amountGoodsInCart));
        Assertions.assertEquals("shopping_cart_badge", classAttrib);

        cart.click();

        List<WebElement> elementsInCart = driver.findElements(By.cssSelector(".cart_item"));

        Assertions.assertEquals(2, elementsInCart.size());

        Thread.sleep(6000);
    }

    @Test
    public void secondBrowserTest() throws InterruptedException {

        driver.get(ConfProperties.getProperty("onlineStore2"));


        WebElement menuButtonCameras = driver.findElement(By.xpath("//*[@id='menu']/div[2]/ul/li[7]/a"));
        menuButtonCameras.click();

        WebElement menuButtonCamera2 = driver.findElement(By.xpath("//*[contains(text(), 'Canon EOS 5D')]/../../../div[2]/button[1]"));
        menuButtonCamera2.click();

//        WebElement addToCardBtn = driver.findElement(By.xpath("//*[@id='button-cart']"));
//        addToCardBtn.click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.titleIs("sdf"));

        WebElement addToCardBtn = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='button-cart']")));
        addToCardBtn.click();

        String titleFromWebDriver = driver.getTitle();
        Assertions.assertEquals("sdf", titleFromWebDriver);
    }

    @Test
    public void thirdBrowserTest() throws InterruptedException {

        driver.get(ConfProperties.getProperty("practiceSite2"));

        WebElement clientSideDelayBtn = driver.findElement(By.xpath("//*[contains(text(), 'Client Side Delay')]"));
        clientSideDelayBtn.click();

        WebElement btn = driver.findElement(By.xpath("//*[@id='ajaxButton']"));
        btn.click();

//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
//        wait.until(ExpectedConditions.textToBePresentInElementLocated(
//                By.xpath("//*[@id='content']/p"),
//                "Data calculated on the client side.")
//        );

        FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver);
        wait.pollingEvery(Duration.ofMillis(100));
        wait.withTimeout(Duration.ofSeconds(35));
        wait.ignoring(NoSuchElementException.class);

        Function<WebDriver, Boolean> predicate = new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver arg0) {
                String displayValText = driver.findElement(By.xpath("//*[@id='content']/p")).getText();
                if (displayValText.equals("Data calculated on the client side."))
                    return true;
                else
                    return false;
            }
        };
        wait.until(predicate);
        Thread.sleep(5000);
//        Function<WebDriver,WebElement> predicate2 = new Function<WebDriver, WebElement>() {
//            public WebElement apply(WebDriver arg0){
//                String displayText = arg0.findElement(By.xpath("//*[@id='content']/p")).getText();
//                if(displayText.equals("Data calculated on the client side.")){
//                    return arg0.findElement(By.xpath("//*[@id='content']/p"));
//                }
//                else{
//                    return null;
//                }
//            }
//        };
//        WebElement someTest = driver.findElement(By.xpath("//*[@id='content']/p"));
//        System.out.println(someTest.getText());
    }

    @Test
    @Owner("some person test12@email.com")
    @Severity(SeverityLevel.BLOCKER)
    public void fifthTest() throws InterruptedException {
        driver.get(ConfProperties.getProperty("practiceSite2"));
        // overlapped button
//        WebElement hiddenLayersBtn = driver.findElement(By.xpath("//*[contains(text(), 'Hidden Layers')]"));
//        hiddenLayersBtn.click();
//
//        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
//        WebElement greenBtn = driver.findElement(By.cssSelector("#greenButton"));
//
//        Thread.sleep(3000);
//        javascriptExecutor.executeScript("document.querySelector('#greenButton').click();");
//        Thread.sleep(3000);
//        javascriptExecutor.executeScript("arguments[0].click()", greenBtn);
//        Thread.sleep(3000);


        WebElement hiddenLayersBtn = driver.findElement(By.xpath("//*[contains(text(), 'Overlapped Element')]"));
        hiddenLayersBtn.click();


        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        WebElement textField = driver.findElement(By.cssSelector("#name"));
        // classic way
//        textField.click();
//        textField.sendKeys("asdasdasdasdasd");
//        Thread.sleep(5000);

        // via js
//        Thread.sleep(3000);
//        javascriptExecutor.executeScript("document.querySelector('#id').value = '123';");
//        Thread.sleep(3000);
//        javascriptExecutor.executeScript("document.querySelector('#name').value = '123SomeText';");
//        Thread.sleep(3000);
//        javascriptExecutor.executeScript("arguments[0].click()", greenBtn);
//        Thread.sleep(3000);

    }


    @Test
    public void sixthTest() throws InterruptedException {
        driver.get("http://tutorialsninja.com/demo/index.php?route=product/product&product_id=42");

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("document.getElementsByName('option[223][]')[0].checked=true;");
        Thread.sleep(2000);

        String docTitle = javascriptExecutor.executeScript("return document.title").toString();
        Assertions.assertEquals("Apple Cinema 30", docTitle);
        String docDomain = javascriptExecutor.executeScript("return document.domain").toString();
        Assertions.assertEquals(ConfProperties.getProperty("domainTest"), docDomain);
        javascriptExecutor.executeScript("window.scrollBy(0, document.body.scrollHeight)");
        Thread.sleep(2000);
    }

    @Test
    public void forthBrowserTest() throws InterruptedException {

        driver.get(ConfProperties.getProperty("practiceSite2"));

        WebElement clientSideDelayBtn = driver.findElement(By.xpath("//*[contains(text(), 'Load Delay')]"));
        clientSideDelayBtn.click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

        WebElement btn = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("[class='btn btn-primary']")));
        btn.click();

//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
//        wait.until(ExpectedConditions.textToBePresentInElementLocated(
//                By.xpath("//*[@id='content']/p"),
//                "Data calculated on the client side.")
//        );
//        Data loaded with AJAX get request.
    }

    @Test
    public void simpleAlert() throws InterruptedException {
        driver.get(ConfProperties.getProperty("practiceSite2"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("setTimeout(function() {alert('sd')}, 3000)");
        Thread.sleep(3000);
        driver.switchTo().alert().accept();
//        driver.switchTo().alert().dismiss();
        Thread.sleep(2000);
        WebElement clientSideDelayBtn = driver.findElement(By.xpath("//*[contains(text(), 'Load Delay')]"));
        clientSideDelayBtn.click();
        Thread.sleep(4000);
    }

    @Test
    public void confirmAlertTest() throws InterruptedException, IOException {
        driver.get(ConfProperties.getProperty("practiceSite2"));



        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("document.test = confirm('ALARMA! YEs or No?')");
        Thread.sleep(1000);
        driver.switchTo().alert().accept();
//        driver.switchTo().alert().dismiss();


        Boolean check = (boolean) javascriptExecutor.executeScript("return document.test");
        Assertions.assertTrue(check);

        WebElement clientSideDelayBtn = driver.findElement(By.xpath("//*[contains(text(), 'Load Delay')]"));
        clientSideDelayBtn.click();


        Thread.sleep(1000);
    }

    @Test
    public void promptAlertTest() throws InterruptedException {
        driver.get(ConfProperties.getProperty("practiceSite2"));

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("document.test = prompt('Give me your data!!!')");
        Thread.sleep(2000);

        driver.switchTo().alert().sendKeys("KryaKrya!");
        Thread.sleep(2000);
        driver.switchTo().alert().accept();
//        driver.switchTo().alert().dismiss();
        Thread.sleep(2000);

        String check = (String) javascriptExecutor.executeScript("return document.test");
        Assertions.assertEquals("KryaKrya!", check);

        WebElement clientSideDelayBtn = driver.findElement(By.xpath("//*[contains(text(), 'Load Delay')]"));
        clientSideDelayBtn.click();
        Thread.sleep(4000);
    }

    @Test
    public void alertsTests() throws InterruptedException {
        driver.get("https://demo.guru99.com/test/delete_customer.php");

        WebElement formEnterData = driver.findElement(By.xpath("//*[@name= 'cusid']"));
        formEnterData.sendKeys("123");
        WebElement submitBtn = driver.findElement(By.xpath("//*[@name= 'submit']"));
        submitBtn.click();
        Thread.sleep(2000);
        driver.switchTo().alert().accept();

        Thread.sleep(3000);
        try{
            driver.switchTo().alert().accept();
        }
        catch (UnhandledAlertException e){
            Alert alert = driver.switchTo().alert();
            String textFromAlert = alert.getText();
            alert.accept();
            Assertions.assertEquals("Customer Successfully Delete!", textFromAlert);
        }

    }

    @Test
    public void windowHandleTest() throws InterruptedException {
        driver.get("https://demo.guru99.com/popup.php");

        WebElement submitBtn = driver.findElement(By.xpath("/html/body/p/a"));
        submitBtn.click();
        submitBtn.click();
        submitBtn.click();
        Thread.sleep(2000);
        String mainTab = driver.getWindowHandle();
        Set<String> tabs = driver.getWindowHandles();
        for(String tab: tabs){
            driver.switchTo().window(tab);
            Thread.sleep(3000);
            driver.switchTo().window(tab).close();

        }
        Thread.sleep(2000);
    }

    @Test
    public void cookieTest(){
        driver.get("https://demoqa.com/frames");
        Set<Cookie> cookies = driver.manage().getCookies();
//        System.out.println(cookies);

        driver.manage().addCookie(new Cookie("Auth", "123"));
        Set<Cookie> cookies1 = driver.manage().getCookies();

//        for(Cookie cookie: cookies1){
//            System.out.println(cookie.getName());
//            System.out.println(cookie.getValue());
//        }
        Cookie cook = new Cookie("Auth", "123");
        driver.manage().deleteCookie(cook);


        driver.manage().addCookie(new Cookie.Builder(cook.getName(), cook.getValue() + "123")
                .domain(cook.getDomain())
                .expiresOn(cook.getExpiry())
                .isSecure(cook.isSecure())
                .build());

        Set<Cookie> cookies2 = driver.manage().getCookies();

        for(Cookie cookie: cookies2){
            System.out.println(cookie.getName());
            System.out.println(cookie.getValue());
        }

    }

    @Step("Проверка суммы чисел {num1} and {num2} и сумма должна быть {expectedSum}")
    public static void checkSumStep(int num1, int num2, int expectedSum){
        Assertions.assertEquals(expectedSum, num1+num2, "Сумма слагаемых не соотв ожид значению");
    }

    @Step("Проверка разности чисел {num1} and {num2}")
    public static void checkDifStep(int num1, int num2, int expectedSum){
        Assertions.assertEquals(expectedSum, num1-num2, "Сумма слагаемых не соотв ожид значению");
    }

    @Step("Проверка синуса числа {num1}")
    public static void checkSinStep(double num1, double expectedSin){
        Assertions.assertEquals(expectedSin, Math.sin(num1), "не совпадает синусы");
    }

    @Epic(value = "Математика")
    @Feature(value = "Добавление товара в корзину")
    @Story(value = "Проверка добавления в корзину через кнопку на главной странице")
    @Test
    public void sumTest1(){
        // здесь мы начали тест
        checkSumStep(2,2,4);
    }

    @Epic(value = "Математика")
    @Story(value = "Вычитания")
    @Feature(value = "Простая мат операция")
    @Test
    public void difTest1(){
        checkDifStep(5,2,3);
    }

    @Epics(value = {@Epic(value = "Математика"), @Epic(value = "Геометрия")})
    @Stories(value = {@Story(value = "Синус"), @Story(value = "Синусоида")})
    @Features(value = {@Feature(value = "Простая мат операция"), @Feature(value = "Тригонометрии")})
    @Test
    public void simpleAllTest(){
        checkSumStep(2,3,5);
        checkSinStep(0,1);
    }


    @Test
    public void simpleTest(){
        checkSumStep(2,3,5);
        checkDifStep(9,2,7);
        checkSumStep(3,3,5);

    }

    @Attachment
    public static byte[] getBytes(String resourceName) throws IOException {
        return Files.readAllBytes(Paths.get("src/test/resources", resourceName));
    }

    @Attachment(value = "Вложение", type = "application/json", fileExtension = ".txt")
    public static byte[] getBytesWithAnnotation(String resourceName) throws IOException {
        return Files.readAllBytes(Paths.get("src/test/resources", resourceName));
    }

    @Attachment(value = "Screenshot", type = "image/png")
    public byte[] makeScreenshot(){
        return ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
    }

    @Step("проверка эквивалентности двух строк")
    public void checkStringEquals(String actual) throws IOException {
        String expected = "This is a sample page";
        getBytes("cat.jpg1");
        getBytes("test.txt");
        getBytesWithAnnotation("test.txt");
        getBytesWithAnnotation("duck.jpg");
        makeScreenshot();
        Assertions.assertEquals(expected, actual);
    }

    @Step("добавить ссылку на ya.ru")
    public static void addLink(){
        String link = "http://ya.ru";
        Allure.addAttachment("Результат", "text/plain", link);
    }

    @Test
    @Flaky
    @Link(name = "это какая-то ссылка", url = "http://ya.ru")
    @Description("здесь мы проверяем, что переключаемся на фрейм и получаем из него текст по локатору")
    public void iFrameTest() throws IOException {
        driver.get("https://demoqa.com/frames");
        //Switch by frame name
        driver.switchTo().frame("frame1");
        WebElement clientSideDelayBtn = driver.findElement(By.cssSelector("#sampleHeading"));
        checkStringEquals(clientSideDelayBtn.getText());
        System.out.println(clientSideDelayBtn.getText());
        addLink();
        driver.switchTo().defaultContent();
    }


    @Test
    public void confirmAlertTest1() throws InterruptedException, IOException {
        driver.get(ConfProperties.getProperty("practiceSite2"));

        ExtentReportsTest.setReport("C:\\az data\\Projects\\IdeaProjects\\testproject\\src\\test\\resources\\default.html");
        ExtentReportsTest.createTest("myfirstReport", "good test");
        ExtentReportsTest.setDriver(driver);

        ExtentReportsTest.test().info("driver set");
        ExtentReportsTest.test();

        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("document.test = confirm('ALARMA! YEs or No?')");
        Thread.sleep(1000);
        driver.switchTo().alert().accept();
//        driver.switchTo().alert().dismiss();

        ExtentReportsTest.shotToDisc("C:\\az data\\Projects\\IdeaProjects\\testproject\\src\\test\\resources\\blabla.png");

        Boolean check = (boolean) javascriptExecutor.executeScript("return document.test");
        Assertions.assertTrue(check);

        WebElement clientSideDelayBtn = driver.findElement(By.xpath("//*[contains(text(), 'Load Delay')]"));
        clientSideDelayBtn.click();

        ExtentReportsTest.getLog();
        ExtentReportsTest.flush();

        Thread.sleep(1000);
    }


    @AfterEach
    public void tearDown(){
        driver.manage().deleteAllCookies();
        driver.close();
        driver.quit();
    }




}
