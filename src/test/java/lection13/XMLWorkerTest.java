//package lection13;
//
//import org.junit.jupiter.api.Test;
//
//import java.io.IOException;
//import java.util.ArrayList;
//
//import static org.junit.jupiter.api.Assertions.*;
//
//
//public class XMLWorkerTest {
//    public static String pathJSON = "src\\main\\resources\\simple_book.json";
//
//    @Test
//    void shouldReturnArrayListWithBooksTest() throws IOException {
//
//        // given
//        Books books = new Books();
//        books.setBooks(new ArrayList<>(){{
//                           add(new Book("Marco Polo", "Everyday Italian", 30, 2005));
//                           add(new Book("Leo Tolstoy", "War And Peace", 10500, 1863));
//                           add(new Book("Автор 123", "Java for Beginner", 123, 2005));
//                       }}
//        );
//
//        //when
//        XMLWorker xmlWorker = new XMLWorker();
//        Books booksFromJson = xmlWorker.deserializeJSON(pathJSON);
//
//        //then
//        assertAll(
//                () -> assertNotNull(booksFromJson),
//                () -> assertEquals(books.getBooks().size(), booksFromJson.getBooks().size()),
//                () -> assertEquals(books.getBooks().get(0).getAuthor(),
//                        booksFromJson.getBooks().get(0).getAuthor())
//        );
//
//    }
//
//    @Test
//    void assertSomeTest(){
//        assertTrue("True".equals("True"));
//    }
//
//}
