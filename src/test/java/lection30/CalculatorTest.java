//package lection30;
//
//import org.junit.jupiter.api.DisplayName;
//import org.testng.Assert;
//import org.testng.annotations.*;
//
//public class CalculatorTest {
//
//    @BeforeSuite
//    public void beforeSuite(){
//        System.out.println("@BeforeSuite");
//    }
//
//    @BeforeClass
//    public void beforeClass(){
//        System.out.println("@BeforeClass");
//    }
//
//    @BeforeTest
//    public void beforeTest(){
//        System.out.println("@BeforeTest");
//    }
//
//    @BeforeMethod
//    public void beforeMethod(){
//        System.out.println("@BeforeMethod");
//    }
//
//
//    @AfterMethod
//    public void afterMethod(){
//        System.out.println("@AfterMethod");
//    }
//
//    @AfterTest
//    public void afterTest(){
//        System.out.println("@AfterTest");
//    }
//
//    @AfterClass
//    public void afterClass(){
//        System.out.println("@AfterClass");
//    }
//
//    @AfterSuite
//    public void afterSuite(){
//        System.out.println("@AfterSuite");
//    }
//
//    @Test(groups="group1")
//    public void testShouldReturnTrueWhenDietRecommended() {
//        // given - Дано, контекст
//        double weight = 150.0;
//        double height = 1.92;
//
//        // when - когда, условие, что тестируем
//        boolean recommended = BMICalculator.isDietRecommended(weight, height);
//
//        //then - Тогда, результат
//        Assert.assertTrue(recommended);
//    }
//
//    @Test(groups="group1")
//    public void shouldReturnFalseWhenDietNotRecommended() {
//        // given - Дано, контекст
//        double weight = 80.0;
//        double height = 1.92;
//
//        // when - когда, условие, что тестируем
//        boolean recommended = BMICalculator.isDietRecommended(weight, height);
//
//        //then - Тогда, результат
//        Assert.assertFalse(recommended);
//    }
//
//    @Test(groups="group1", expectedExceptions = ArithmeticException.class)
//    public void shouldThrowArithmeticExceptionWhenHeightZero(){
//        // given
//        double weight = 50.0;
//        double height = 0.0;
//
//        //when - then
//        BMICalculator.isDietRecommended(weight, height);
//
//    }
//
//    @Test(testName = "Some test name",
//            groups="group1",
//            timeOut = 1000,
//            dependsOnMethods = {"testShouldReturnTrueWhenDietRecommendedParameterizedCSVSource"}
//    ) //enabled = false,
//    @Parameters({"coderWeight"})
//    public void testShouldReturnTrueWhenDietRecommendedParameterized(String coderWeight){
//        // given - Дано, контекст
//        double weight = Double.valueOf(coderWeight);
//        double height = 1.92;
//
//        // when - когда, условие, что тестируем
//        boolean recommended = BMICalculator.isDietRecommended(weight, height);
//
//        //then - Тогда, результат
//        Assert.assertTrue(recommended);
//    }
//
//    @DataProvider(name = "provideNumbers")
//    public Object[][] provideData(){
//        return new Object[][] {
//                {89.0, 1.72},
//                {95.0, 1.75},
//                {110.0, 1.75}
//        };
//    }
//
//    @Test(groups="group1", dataProvider = "provideNumbers")
//    @Parameters({"coderWeight", "coderHeight"})
//    public void testShouldReturnTrueWhenDietRecommendedParameterizedCSVSource(double coderWeight,
//                                                                              double coderHeight){
//        // given - Дано, контекст
//        double weight = coderWeight;
//        double height = coderHeight;
//
//        // when - когда, условие, что тестируем
//        boolean recommended = BMICalculator.isDietRecommended(weight, height);
//
//        //then - Тогда, результат
//        Assert.assertTrue(recommended);
//    }
//
//    @Test(groups="group3")
//    public void test1() {
//        System.out.println("test1 from group3");
//        Assert.assertTrue(true);
//    }
//
//    @Test(groups="group3")
//    public void test2() {
//        System.out.println("test2 from group3");
//        Assert.assertTrue(true);
//    }
//
//    @Test(groups="group2")
//    public void test3() {
//        System.out.println("test3 from group2");
//        Assert.assertTrue(true);
//    }
//
//    @Test(groups="group2")
//    public void test4() {
//        System.out.println("test4 from group2");
//        Assert.assertTrue(true);
//    }
//
//}
