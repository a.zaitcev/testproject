package lection612;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Configuration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache;

public class TestSearchPOM {

    SearchPage searchPage = new SearchPage();
    ResultsPage resultsPage = new ResultsPage();
    SeleniumTestPage seleniumTestPage = new SeleniumTestPage();
    SeleniumPageFormDemo seleniumPageFormDemo = new SeleniumPageFormDemo();
    SeleniumTablesPage seleniumTablesPage = new SeleniumTablesPage();


    @BeforeEach
    public void setup(){
        clearBrowserCache();
//        Configuration.baseUrl = "https://yandex.ru/";
        Configuration.browserSize = "1920x1080";
        Configuration.baseUrl = "https://demo.seleniumeasy.com/";
    }

    @Test
    public void userCanSearch() throws InterruptedException {
        searchPage.open();
        resultsPage = searchPage.searchPage("selenide");
        resultsPage.getResults().shouldHave(CollectionCondition.sizeGreaterThan(10));
        resultsPage.getResults().get(2).shouldHave(text("Selenide: concise UI tests in Java"));
        Thread.sleep(5000);
    }

    @Test
    public void firstTest() throws InterruptedException {
        seleniumTestPage.open("https://demo.seleniumeasy.com/");
        seleniumTestPage.closePopupIfExist();
        seleniumTestPage.openSimpleFormsPage();
        seleniumTestPage.shouldHaveTitle("Selenium Easy Demo - Simple Form to Automate using Selenium");

        seleniumPageFormDemo.enterData("test");
        seleniumPageFormDemo.checkEnteredData("labelResult", "test");

        seleniumPageFormDemo.enterSumData(2,2);
        seleniumPageFormDemo.checkEnteredData("labelResultSum", "4");

        Thread.sleep(5000);
    }

    @Test
    public void secondTest() throws InterruptedException {
        seleniumTestPage.open("https://demo.seleniumeasy.com/");
        seleniumTestPage.closePopupIfExist();
        seleniumTestPage.openSimpleFormsPage();
        seleniumTestPage.shouldHaveTitle("Selenium Easy Demo - Simple Form to Automate using Selenium");

        seleniumPageFormDemo.enterData("test");
        seleniumPageFormDemo.checkEnteredData("labelResult", "test");

        seleniumPageFormDemo.enterSumData("a","a");
        seleniumPageFormDemo.checkEnteredData("labelResultSum", "NaN");

        Thread.sleep(5000);
    }

    @Test
    public void thirdTest() throws InterruptedException {
        seleniumTestPage.open("https://demo.seleniumeasy.com/");
        seleniumTestPage.closePopupIfExist();
        seleniumTestPage.openTablePage();
        seleniumTablesPage.clickFilterBtn("btnGreen");
        seleniumTablesPage.checkEntries("greenRows",2);
        Thread.sleep(2000);
    }

}
