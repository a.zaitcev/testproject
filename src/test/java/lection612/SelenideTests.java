package lection612;

import com.codeborne.selenide.*;
import io.qameta.allure.Description;
import Helpers.ConfProperties;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;


import java.time.Duration;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.webdriver;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.clearBrowserCache;
import static com.codeborne.selenide.WebDriverConditions.url;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SelenideTests {

    @BeforeEach
    public void setUP(){
        Configuration.timeout = 10;
        clearBrowserCache();
    }

    @Test
    public void firstTest() throws InterruptedException {
        open("http://uitestingplayground.com/");
        Thread.sleep(5000);
    }

    @Test
    public void secondTest() throws InterruptedException {
        open(ConfProperties.getProperty("startPage"));

        $(By.cssSelector("#user-name")).setValue("standard_user");
        $(By.cssSelector("#password")).setValue("secret_sauce");
        $(By.xpath("//*[@data-test='login-button']")).click();

        webdriver().shouldHave(url("https://www.saucedemo.com/inventory.html"));
        webdriver().shouldHave(url("https://www.saucedemo.com/inventory.html"), Duration.ofSeconds(30));
        webdriver().shouldNotHave(url("http://ya.ru"));

        $(By.xpath("//*[contains(text(), 'Sauce Labs Fleece Jacket')]/../../../div[2]/button")).click();
        $(By.xpath("//*[contains(text(), 'Sauce Labs Bike Light')]/../../../div[2]/button")).click();
        $(By.xpath("//*[@data-test='product_sort_container']")).selectOption(1);

        SelenideElement cartBtn = $(By.cssSelector(".shopping_cart_badge"));

        cartBtn.shouldHave(text("2"));
        cartBtn.shouldHave(attribute("innerText", "2"));

        cartBtn.shouldNotHave(text("3"));
        cartBtn.shouldNotHave(exactValue("3"));

        cartBtn.shouldHave(attribute("class"));
        cartBtn.shouldNotHave(attribute("href"));

        cartBtn.shouldHave(attribute("class", "shopping_cart_badge"));
        cartBtn.shouldHave(cssClass("shopping_cart_badge"));

        cartBtn.click(ClickOptions.usingJavaScript());

        ElementsCollection elementsInCart = $$(By.cssSelector(".cart_item"));
        elementsInCart.shouldHave(CollectionCondition.size(2));

        $(By.cssSelector("#user-name")).shouldNotBe(visible);
//        $(By.cssSelector("#user-name")).should(disabled);
//        $(By.cssSelector("#user-name")).should(focused);


        webdriver().shouldHave(url("https://www.saucedemo.com/cart.html"));
        $(By.cssSelector(".title")).shouldHave(text("YOUR CART"));
        $(By.cssSelector("#checkout")).shouldHave(cssValue("background-color","rgba(226, 35, 26, 1)"));
        $(By.cssSelector("#checkout")).shouldHave(cssValue("font-size","14px"));

        Thread.sleep(5000);
    }

    @Test
    @Description("проверяем задержки в selenide")
    public void thirdTest() throws InterruptedException {

        open(ConfProperties.getProperty("onlineStore2"));

        $(By.xpath("//*[@id='menu']/div[2]/ul/li[7]/a")).click();
        $(By.xpath("//*[contains(text(), 'Canon EOS 5D')]/../../../div[2]/button[1]")).click();
        screenshot("my_file_name");

        WebDriverWait wait = new WebDriverWait(webdriver().driver().getWebDriver(), Duration.ofSeconds(5));
        wait.until(ExpectedConditions.titleIs("sdf"));

        $(By.xpath("//*[@id='button-cart']")).should(visible, Duration.ofMillis(50)).click();

        String titleFromWebDriver = webdriver().driver().getWebDriver().getTitle();
        Assertions.assertEquals("sdf", titleFromWebDriver);
    }
}
