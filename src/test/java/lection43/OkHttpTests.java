//package lection43;
//
//import okhttp3.*;
//import org.json.JSONObject;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import java.io.IOException;
//
//public class OkHttpTests {
//    public static String BASE_URL = "https://petstore.swagger.io/v2";
//
//    OkHttpClient client;
//
//    @BeforeEach
//    public void init(){
//        client = new OkHttpClient();
//    }
//
//
//    @Test
//    public void sendPostRequestToPetStore() throws IOException{
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("id", "124");
//        jsonObject.put("name", "goodBoy!");
//        jsonObject.put("status", "hungry!");
//
//        JSONObject jsonObjectCategory = new JSONObject();
//        jsonObjectCategory.put("id", "1");
//        jsonObjectCategory.put("name", "megaRockDog!");
//
//        jsonObject.put("category", jsonObjectCategory);
//
//
//        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//
//        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
//
//        Request request = new Request.Builder()
//                .url(BASE_URL + "/pet")
//                .addHeader("Content-Type", "application/json")
//                .addHeader("accept","application/json")
//                .post(body)
//                .build();
//
//        Call call = client.newCall(request);
//
//        Response response = call.execute();
//
//        ResponseBody responseBody = response.body();
//
//        String responceString = responseBody.string();
//
//        System.out.println(responceString);
//        Assertions.assertEquals(200, response.code());
//    }
//
//    @Test
//    public void getCreatedPet() throws IOException {
//        Request request = new Request.Builder()
//                .url(BASE_URL + "/pet/124")
//                .get()
//                .build();
//        Call call = client.newCall(request);
//        Response response = call.execute();
//
//        Headers headers = response.headers();
//        System.out.println(headers.get("content-type"));
//
//        String jsonData = response.body().string();
//        System.out.println(response.message().toString());
//
////        JSONObject jsonObject = new JSONObject(jsonData);
////        System.out.println(jsonObject.get("id"));
////        System.out.println(jsonObject.getString("name"));
////        System.out.println(jsonObject.getString("status"));
//
////        JSONObject getSubArray = jsonObject.getJSONObject("category");
//
//        Assertions.assertEquals(200, response.code());
////        Assertions.assertEquals("goodBoy!", jsonObject.getString("name"));
////        Assertions.assertEquals("megaRockDog!", getSubArray.getString("name"));
//    }
//
//    @Test
//    public void updatePet() throws IOException {
//        JSONObject jsonObject = new JSONObject();
//        jsonObject.put("id", "124");
//        jsonObject.put("name", "badBoy!");
//
//        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
//
//        RequestBody body = RequestBody.create(JSON, jsonObject.toString());
//
//        Request request = new Request.Builder()
//                .url(BASE_URL + "/pet")
//                .addHeader("Content-Type", "application/json")
//                .addHeader("accept","application/json")
//                .put(body)
//                .build();
//
//        Call call = client.newCall(request);
//
//        Response response = call.execute();
//
//        Headers headers = response.headers();
//        System.out.println(headers.get("content-type"));
//
//        String jsonData = response.body().string();
//
//        JSONObject jsonObjectResponce = new JSONObject(jsonData);
//        System.out.println(jsonObjectResponce.get("id"));
//        System.out.println(jsonObjectResponce.getString("name"));
//        System.out.println(jsonObjectResponce.getString("status"));
//
//
//        Assertions.assertEquals(200, response.code());
//        Assertions.assertEquals("badBoy!", jsonObjectResponce.getString("name"));
//    }
//
//    @Test
//    public void deletePet() throws IOException {
//        Request request = new Request.Builder()
//                .url(BASE_URL + "/pet/124")
//                .delete()
//                .build();
//
//        Call call = client.newCall(request);
//
//        Response response = call.execute();
//
//        Assertions.assertEquals(200, response.code());
//    }
//
//}
