package lection612;

import Helpers.JsonFileReader;
import com.codeborne.selenide.Condition;

public class SeleniumPageFormDemo {
    JsonFileReader jsonFileReader = new JsonFileReader();

    public void enterData(String message){
        jsonFileReader.getElementByAlias("inputFormData").setValue(message);
        jsonFileReader.getElementByAlias("btnShowMessage").click();
    }

    public void checkEnteredData(String alias, String message){
        jsonFileReader.getElementByAlias(alias).shouldHave(Condition.text(message));
    }

    public void enterSumData(int a, int b){
        jsonFileReader.getElementByAlias("sum1InputField").setValue(String.valueOf(a));
        jsonFileReader.getElementByAlias("sum2InputField").setValue(String.valueOf(b));
        jsonFileReader.getElementByAlias("btnCalcResult").click();
    }

    public void enterSumData(String a, String b){
        jsonFileReader.getElementByAlias("sum1InputField").setValue(a);
        jsonFileReader.getElementByAlias("sum2InputField").setValue(b);
        jsonFileReader.getElementByAlias("btnCalcResult").click();
    }

}
