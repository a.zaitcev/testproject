package lection612;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

public class SearchPage {
    private SelenideElement searchBox = $(By.name("text"));

    public ResultsPage searchPage(String text){
        searchBox.setValue(text).pressEnter();
        return page(ResultsPage.class);
    }

    public SearchPage open(){
        Selenide.open("search");
        return this;
    }

}
