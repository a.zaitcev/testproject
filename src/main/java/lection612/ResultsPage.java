package lection612;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$$;

public class ResultsPage {
    private final ElementsCollection results = $$(By.cssSelector(".OrganicTitleContentSpan.organic__title"));

    public ElementsCollection getResults(){
        return $$(results);
    }
}
