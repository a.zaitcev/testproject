package lection612;

import Helpers.JsonFileReader;
import com.codeborne.selenide.CollectionCondition;

public class SeleniumTablesPage {
    JsonFileReader jsonFileReader = new JsonFileReader();

    public void clickFilterBtn(String alias){
        jsonFileReader.getElementByAlias(alias).click();
    }
    public void checkEntries(String alias, int sizeCollection){
        jsonFileReader.getElementsCollectionByAlias(alias).shouldHave(CollectionCondition.size(sizeCollection));
    }

}
