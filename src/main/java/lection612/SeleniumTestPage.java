package lection612;

import Helpers.JsonFileReader;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.webdriver;

public class SeleniumTestPage {
    JsonFileReader jsonFileReader = new JsonFileReader();
//    private SelenideElement noBtn = $(By.xpath("//*[@id='at-cv-lightbox-button-holder']/a[2]"));
//    private SelenideElement startPracticing = $(By.xpath("//*[@id='btn_basic_example']"));
//    private SelenideElement openSimplePageForms = $(By.xpath("//div[@class='list-group']//*[contains(text(),'Simple Form Demo')]"));

    public SeleniumTestPage open(String s){
        Selenide.open(s);
        return this;
    }

    public void closePopupIfExist(){
        jsonFileReader.getElementByAlias("noBtn").shouldBe(Condition.visible, Duration.ofSeconds(5)).click();
    }

    public void openSimpleFormsPage(){
        jsonFileReader.getElementByAlias("startPracticing").click();
        jsonFileReader.getElementByAlias("openSimplePageForms").click();
    }

    public void shouldHaveTitle(String expectedTitle){
        String titleActual = webdriver().driver().getWebDriver().getTitle();
        Assertions.assertEquals(expectedTitle, titleActual);
    }

    public void openTablePage(){
        jsonFileReader.getElementByAlias("tableRowMenu").click();
        jsonFileReader.getElementByAlias("tableSubRowMenu").click();
    }
}
