package Helpers;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.openqa.selenium.By;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class JsonFileReader {
    private static String fileNameJson = "src/test/resources/locators.json";
    public static JSONObject jsonObject;

    public JsonFileReader(){
        try
        {
            FileInputStream fileInputStream = new FileInputStream(fileNameJson);
            JSONTokener tokener = new JSONTokener(fileInputStream);
            jsonObject = new JSONObject(tokener);
        }
        catch (FileNotFoundException e){
            throw new IllegalArgumentException("file not found!" + fileNameJson);

        }
    }

    public static String getLocatorByAlias(String alias){
        return jsonObject.getString(alias);
    }

    public SelenideElement getElementByAlias(String alias){
        return $(By.xpath(JsonFileReader.getLocatorByAlias(alias)));
    }

    public ElementsCollection getElementsCollectionByAlias(String alias){
        return $$(By.xpath(JsonFileReader.getLocatorByAlias(alias)));
    }
}
