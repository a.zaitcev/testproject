//package lection30;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//
//public class ActivityCalculator {
//    private static final int WORKOUT_DURATION_MIN = 45;
//
//    public static String rateActivityLevel(
//            int weeklyCardioMin, int weeklyWorkoutSessions){
//
//        if (weeklyCardioMin < 0 || weeklyWorkoutSessions < 0){
//            throw new RuntimeException("input below zero");
//        }
//
//        int totalMinutes = weeklyCardioMin +  weeklyWorkoutSessions * WORKOUT_DURATION_MIN;
//        double avgDailyMinutes = totalMinutes / 7.0;
//
//        if(avgDailyMinutes < 20){
//            return "bad";
//        }
//        else if (avgDailyMinutes < 40){
//            return "average";
//        }
//
//        return "good";
//    }
//
//    public static void main(String[] args) {
//        ObjectMapper jsonMapper = new ObjectMapper();
//    }
//}
