package lection6;

public class TestString {

    public char[] convertToUpperCase(String s){
        char[] chars = s.toCharArray();

        for (int i = 0; i < chars.length; i++) {
            chars[i] = Character.toUpperCase(chars[i]);

        }
        return chars;
    }

    public static void main(String[] args) {
        // 1
//        String str1 = "Java";
//        String str2 = new String(); // пустая строка
//        String str3 = new String(new char[] {'h', 'e', 'l', 'l', 'o'});
//        String str4 = new String(new char[]{'w', 'e', 'l', 'c', 'o', 'm', 'e'}, 3, 4);//3 -начальный индекс, 4 -кол-во символов
//
//        System.out.println(str1); // Java
//        System.out.println(str2); //
//        System.out.println(str3); // hello
//        System.out.println(str4); // come
//
//        String str5 = "Java";
//        System.out.println(str1.length()); // 4
//
//        // 2
//        String sTest = "some String";
//        TestString testString = new TestString();
//        System.out.println(new String(testString.convertToUpperCase(sTest)));


        // строка может быть пусто
//        String s = "";   // строка не указывает на объект
//        if(s.length() == 0) System.out.println("String is empty");
//
////        String s = "";   // строка не указывает на объект
//        if(s.isEmpty()) System.out.println("String is empty");


        // null не эквивалентно пустой строке. Например, в следующем случае мы столкнемся с ошибкой выполнения:
//        String s = null; // строка не указывает на объект
//        if(s != null && s.length() == 0){
//            System.out.println("string is empty");
//        }





    }
}
