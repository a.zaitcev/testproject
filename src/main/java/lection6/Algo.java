package lection6;

import java.util.Arrays;

public class Algo {
    public static int linearSearch(int arr[], int elementToSearch) {

        for (int index = 0; index < arr.length; index++) {
            System.out.println(arr[index]);
            if (arr[index] == elementToSearch)
                return index;
        }
        return -1;
    }

    public static void swap(int[] array, int ind1, int ind2) {
        int tmp = array[ind1];
        array[ind1] = array[ind2];
        array[ind2] = tmp;
    }

    public static void main(String[] args) {
        // bubble sort
//        int[] array = {10, 2, 10, 3, 1, 2, 5};
//        System.out.println(Arrays.toString(array));
//        boolean needIteration = true;
//        while (needIteration) {
//            needIteration = false;
//            for (int i = 1; i < array.length; i++) {
//                if (array[i] < array[i - 1]) {
//                    swap(array, i, i-1);
//                    needIteration = true;
//                }
//            }
//            System.out.println(Arrays.toString(array));
//
//        }
//        System.out.println(Arrays.toString(array));


        int elementToSearch = 101;
        int index = linearSearch(new int[]{89, 57, 91, 47, 95, 3, 27, 22, 67, 99}, elementToSearch);
        if (index == -1){
            System.out.println(elementToSearch + " not found.");
        }
        else {
            System.out.println(elementToSearch + " found at index: " + index);
        }




    }
}
