package lection6;

public class StringsConcat {

    public static void main(String[] args) {
        // 1
//        String str1 = "Java";
//        String str2 = "Hello";
//        String str3 = str1 + " " + str2;
//
//        System.out.println(str3); // Hello Java
//
//        int year = 2021;
//        String str4 = "Год " + year;
//        System.out.println(str4);


        // 2
//        String str5 = "Java";
//        String str6 = "Hello";
//        String str2 = str6.concat(str5); // HelloJava
//        System.out.println(str2);

        // 3
//        String str1 = "Java";
//        String str2 = "Hello";
//        String str3 = String.join(",", str2, str1, "!", " My Name is Andrei"); // Hello Java
//
//        System.out.println(str3);

        // extract
//        System.out.println(str5.charAt(1));
//
//        String str = "Java";
//        char c = str.charAt(2);
//        System.out.println(c); // v
//
//        String str9 = "Hello world!";
//        int start = 6;
//        int end = 11;
//        char[] dst=new char[end - start];
//        str9.getChars(start, end, dst, 0);
//        System.out.println(dst); // world
    }
}
