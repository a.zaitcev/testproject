package lection6;

import java.util.Arrays;

public class StringB {
    public static void main(String[] args) {
//        StringBuffer()
//        StringBuffer(int capacity)
//        StringBuffer(String str)
//        StringBuffer(CharSequence chars)
//        String str = "Java";
//        StringBuffer strBuffer = new StringBuffer(str);
//        System.out.println("Емкость: " + strBuffer.capacity()); // 20
//        strBuffer.ensureCapacity(32);
//        System.out.println("Емкость: " + strBuffer.capacity()); // 42
//        System.out.println("Длина: " + strBuffer.length()); // 4
//
//        System.out.println(strBuffer.toString()); // Java
//        System.out.println(strBuffer); // Java

//        StringBuffer strBuffer = new StringBuffer("Java");
//        char c = strBuffer.charAt(0); // J
//        System.out.println(c);
//        strBuffer.setCharAt(0, 'c');
//        System.out.println(strBuffer.toString()); // cava

        //append
//        StringBuffer sb = new StringBuffer();
//
//        sb.append(Integer.valueOf(2));
//        sb.append("; ");
//        sb.append(false);
//        sb.append("; ");
//        sb.append(Arrays.asList(1,2,3));
//        sb.append("; ");
//        System.out.println(sb); // 2; false; [1, 2, 3];

        // insert
//        StringBuffer strBuffer = new StringBuffer("word");
//
//        strBuffer.insert(3, 'l');
//        System.out.println(strBuffer.toString()); //world
//
//        strBuffer.insert(0, "s");
//        System.out.println(strBuffer.toString()); //sworld

        // delete
//        StringBuffer strBuffer = new StringBuffer("assembler");
//        strBuffer.delete(0,2);
//        System.out.println(strBuffer.toString()); //sembler
//
//        strBuffer.deleteCharAt(6);
//        System.out.println(strBuffer.toString()); //semble

        // substring
//        StringBuffer strBuffer = new StringBuffer("hello java!");
//        String str1 = strBuffer.substring(6); // обрезка строки с 6 символа до конца
//        System.out.println(str1); //java!
//
//        String str2 = strBuffer.substring(3, 9); // обрезка строки с 3 по 9 символ
//        System.out.println(str2); //lo jav

    }
}
