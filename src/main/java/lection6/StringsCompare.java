package lection6;

public class StringsCompare {
    public static void main(String[] args) {
//        String str1 = "Hello";
//        String str2 = "hello";
//
//        System.out.println(str1.equals(str2)); // false
//        System.out.println(str1.equalsIgnoreCase(str2)); // true
        // compare strings

//        String test = "literal";
//        String test2 = new String("literal");
//        System.out.println(test == test2);
//        System.out.println(System.identityHashCode(test));
//        System.out.println(System.identityHashCode(test2));
//
//        System.out.println(test.equals(test2));
//        "myString".compareTo(null);  //Throws java.lang.NullPointerException
//        "myString".equals(null);     //Returns false

        String str3 = "Hello world";
        String str4 = "I work";
        boolean result = str3.regionMatches(6, str4, 2, 4);
        System.out.println(result); // true

    }

}
