package lection6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpressions {
    public static void main(String[] args) {
        String regex="java"; // шаблон строки ”java”;
//        String regex="\\d{3}"; // шаблон строки из трех цифровых символов;

        // split
//        String text = "FIFA will. never! regret, it";
//        String[] words = text.split("\\s*(\\s|,|!|\\.|\\,)\\s*");
//        for(String word : words){
//            System.out.println(word);
//        }

        //matches
//        String input = "+123434545562";
//        boolean result = input.matches("(\\+*)\\d{12}");
//        if(result){
//            System.out.println("It is a phone number");
//        }
//        else{
//            System.out.println("It is not a phone number!");
//        }
//
//
//        // Pattern
//        String input1 = "Hello";
//        boolean found = Pattern.matches("Hello", input1);
//        if(found)
//            System.out.println("Найдено");
//        else
//            System.out.println("Не найдено");


        // Matcher
        // поиск по шаблону в тексте
//        String input3= "Hello";
//        // создадим объект Pattern с помощью метода compile(), который позволяет установить шаблон:
//        Pattern pattern = Pattern.compile("Hello");
//        // метод matcher(String input), который в качестве параметра принимает строку, где надо проводить поиск,
////        и возвращает объект Matcher
//        Matcher matcher = pattern.matcher(input3);
//        // у объекта Matcher вызывается метод matches() для поиска соответствий шаблону в тексте
//        boolean found1 = matcher.matches();
//        if(found1)
//            System.out.println("Найдено");
//        else
//            System.out.println("Не найдено");


        // мы хотим найти в строке все вхождения слова Java
//        String input4 = "Hello Java! Hello JavaScript! JavaSE 8.";
//        Pattern pattern = Pattern.compile("Java(\\w*)");
//        Matcher matcher = pattern.matcher(input4);
//        while(matcher.find())
//            System.out.println(matcher.group());


        // replace
        //Теперь сделаем замену всех совпадений с помощью метода replaceAll():
//        String input8 = "Hello Java! Hello JavaScript! JavaSE 8.";
//        Pattern pattern = Pattern.compile("Java(\\w*)");
//        Matcher matcher = pattern.matcher(input8);
//        String newStr = matcher.replaceAll("HTML");
//        System.out.println(newStr); // Hello HTML! Hello HTML! HTML 8.

//        Также надо отметить, что в классе String также имеется метод replaceAll() с подобным действием:
//        String input5 = "Hello Java! Hello JavaScript! JavaSE 8.";
//        String myStr =input5.replaceAll("Java(\\w*)", "HTML");
//        System.out.println(myStr); // Hello HTML! Hello HTML! HTML 8.



        // split
//        String input8 = "Hello Java! Hello JavaScript! JavaSE 8.";
//        Pattern pattern1 = Pattern.compile("[ ,.!?]");
//        String[] words1 = pattern1.split(input8);
//        for(String word:words1)
//            System.out.println(word);


    }
}
