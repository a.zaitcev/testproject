package lection6;

import java.util.Arrays;
import java.util.Scanner;

public class OtherStringsMethods {
    public static void main(String[] args) {
//        String str = "Hello world";
//        int index1 = str.indexOf('l'); // 2
//        int index2 = str.indexOf("wo"); //6
//        int index3 = str.lastIndexOf('l'); //9
//        System.out.println(index1);
//        System.out.println(index2);
//        System.out.println(index3);

//        String str = "myfile.exe";
//        boolean start = str.startsWith("my"); //true
//        boolean end = str.endsWith("exe"); //true
//        System.out.println(start);
//        System.out.println(end);


//        String str = "Hello world";
//        String replStr1 = str.replace('l', 'd'); // Heddo wordd
//        String replStr2 = str.replace("Hello", "Bye"); // Bye world
//        System.out.println(replStr1);
//        System.out.println(replStr2);


//        Scanner sc = new Scanner(System.in);
//        String str = "  hello world  ";
//        String str = sc.nextLine();
//        System.out.println(str);
//        str = str.trim(); // hello world
//        System.out.println(str);
//
//
//        String str = "Hello world";
//        String substr1 = str.substring(6); // world
//        String substr2 = str.substring(2,4); //lo
//        System.out.println(substr1);
//        System.out.println(substr2);
//
//
//        String str = "Hello World";
//        System.out.println(str.toLowerCase()); // hello world
//        System.out.println(str.toUpperCase()); // HELLO WORLD
//
//
        String text = "FIFA,will,never,regret,it";
        String[] words = text.split(",");
        for(String word : words){
            System.out.println(word);
        }
        System.out.println(Arrays.toString(words));
    }
}
