//package lection13;
//
//
//import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
//import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class Books {
//
//    @JacksonXmlProperty(localName = "book")
//    @JacksonXmlElementWrapper(useWrapping = false)
//    public List<Book> books = new ArrayList<>();
//
//    public Books(){
//        super();
//    }
//
//    public List<Book> getBooks() {
//        return books;
//    }
//
//    public void setBooks(List<Book> books) {
//        this.books = books;
//    }
//}
