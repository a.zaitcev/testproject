//package lection13;
//
//
//import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
//import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
//
//@JacksonXmlRootElement(localName = "Book")
//public class Book {
//
//    @JacksonXmlProperty(localName = "id")
//    private static int count = 0;
//    private int id=0;
//
//    @JacksonXmlProperty(isAttribute = true)
//    private String ns = "someText";
//
//    @JacksonXmlProperty(localName = "Author")
//    private String author;
//
//    @JacksonXmlProperty(localName = "Title")
//    private String title;
//
//    @JacksonXmlProperty(localName = "Price")
//    private int price;
//
//    @JacksonXmlProperty(localName = "Year")
//    private int year;
//
//
//    public Book(String author, String title, int price, int year) {
//        this.author = author;
//        this.title = title;
//        this.price = price;
//        this.year = year;
//        this.id = count++;
//    }
//
//    public static int getCount() {
//        return count;
//    }
//
//    public static void setCount(int count) {
//        Book.count = count;
//    }
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
//    public Book(){
//        super();
//    }
//
//    public String getAuthor() {
//        return author;
//    }
//
//    public void setAuthor(String author) {
//        this.author = author;
//    }
//
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public int getPrice() {
//        return price;
//    }
//
//    public void setPrice(int price) {
//        this.price = price;
//    }
//
//    public int getYear() {
//        return year;
//    }
//
//    public void setYear(int year) {
//        this.year = year;
//    }
//
//    public String getNs() {
//        return ns;
//    }
//
//    public void setNs(String ns) {
//        this.ns = ns;
//    }
//}
