//package lection13;
//
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.core.type.TypeReference;
//import com.fasterxml.jackson.databind.JsonNode;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.ObjectWriter;
//import com.fasterxml.jackson.databind.SerializationFeature;
//import com.fasterxml.jackson.dataformat.xml.XmlMapper;
//import com.fasterxml.jackson.dataformat.xml.ser.ToXmlGenerator;
//import org.w3c.dom.Document;
//import org.w3c.dom.Node;
//import org.w3c.dom.NodeList;
//import org.xml.sax.SAXException;
//
//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;
//import javax.xml.parsers.ParserConfigurationException;
//import java.io.*;
//import java.util.ArrayList;
//import java.util.LinkedHashMap;
//import java.util.Map;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//
//public class XMLWorker {
//    public static String pathXML = "src\\main\\resources\\simple_book.xml";
//    public static String pathJSON = "src\\main\\resources\\simple_book.json";
//
//
//    public static void main(String[] args) throws IOException {
//
//        Books books = new Books();
//        books.setBooks(new ArrayList<>(){{
//                           add(new Book("Giada De Laurentiis", "Everyday Italian", 30, 2005));
//                           add(new Book("Leo Tolstoy", "War And Peace", 10500, 1863));
//                           add(new Book("Автор 123", "Java for Beginner", 123, 2005));
//                       }}
//        );
//
//        // class to xml/json
//        XMLWorker xmlWorker = new XMLWorker();
//        xmlWorker.serializeClass(books, pathXML, pathJSON);
//
//
//        // deserialize xml
//        Books booksDeserializedXML = xmlWorker.deserializeXML(pathXML);
//
//        assertTrue(booksDeserializedXML.getBooks().get(0).getAuthor().equals("Giada De Laurentiis"));
//
//        booksDeserializedXML.getBooks().get(0).setAuthor("Marco Polo");
//        booksDeserializedXML.getBooks().get(0).setPrice(50);
//
//
////        System.out.println(booksDeserializedXML.getBooks().get(0).getAuthor());
//
//        xmlWorker.serializeClass(booksDeserializedXML, pathXML, pathJSON);
//
//
////        // deserialize json
//        Books booksDeserializedJSON = xmlWorker.deserializeJSON(pathJSON);
//        assertTrue(booksDeserializedJSON.getBooks().get(0).getAuthor().equals("Marco Polo"));
//        assertTrue(booksDeserializedJSON.getBooks().get(0).getPrice() == 50);
//
////        System.out.println(booksDeserializedJSON.getBooks().get(0).getAuthor());
////        System.out.println(booksDeserializedJSON.getBooks().get(0).getPrice());
////
////
//        xmlWorker.deserializeRawXML();
////
////        Map<String, Object> testMap = xmlWorker.deserializeUnknownJSONFile();
////        for (Map.Entry<?, ?> entry : testMap.entrySet()) {
////            System.out.println(entry.getKey() + " = " + entry.getValue());
////        }
//
//
//
//    }
//
//
//    public void serializeClass(Books books, String pathXML, String pathJSON) throws IOException {
//
//        XmlMapper xmlMapper = new XmlMapper();
//
//        // to xml
//        xmlMapper.enable(SerializationFeature.INDENT_OUTPUT);
//        xmlMapper.configure(ToXmlGenerator.Feature.WRITE_XML_DECLARATION, true);
//        xmlMapper.writeValue(new File(pathXML), books);
//
//
//        // to json
//        ObjectWriter mapper = new ObjectMapper().writer().withDefaultPrettyPrinter();
//        String json = mapper.writeValueAsString(books);
//        mapper.writeValue(new File(pathJSON), books);
//
//
//        File fileXML = new File(pathXML);
//        assertNotNull(fileXML);
//
//        File fileJSON = new File(pathJSON);
//        assertNotNull(fileJSON);
//    }
//
//    public void deserializeRawXML(){
//        {
//            try {
//                // Создается построитель документа
//                DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
//                // Создается дерево DOM документа из файла
//                Document document = documentBuilder.parse(pathXML);
//
//                // Получаем корневой элемент
//                Node root = document.getDocumentElement();
//
//                System.out.println("List of books:");
//                System.out.println();
//                // Просматриваем все подэлементы корневого - т.е. книги
//                NodeList books = root.getChildNodes();
//
//                // пример получения доступа к тегу Author и его значению в xml-документе
//                // получаем все подэлементы у первого подэлемента после корня
//                NodeList listSubrootElements = document.getDocumentElement().getChildNodes().item(1).getChildNodes();
//
//                // затем берем у него второго потомка
//                String tag = listSubrootElements.item(3).getNodeName();
//                String value = listSubrootElements.item(3).getChildNodes().item(0).getTextContent();
//                System.out.println("value of tag: " + tag + " equals " + value);
//
//                for (int i = 0; i < books.getLength(); i++) {
//                    Node book = books.item(i);
//                    // Если нода не текст, то это книга - заходим внутрь
//                    if (book.getNodeType() != Node.TEXT_NODE) {
//                        NodeList bookProps = book.getChildNodes();
//                        for(int j = 0; j < bookProps.getLength(); j++) {
//                            Node bookProp = bookProps.item(j);
//                            // Если нода не текст, то это один из параметров книги - печатаем
//                            if (bookProp.getNodeType() != Node.TEXT_NODE) {
//                                System.out.println(bookProp.getNodeName() + ":" + bookProp.getChildNodes().item(0).getTextContent());
//                            }
//                        }
//                        System.out.println("===========>>>>");
//                    }
//                }
//
//            } catch (ParserConfigurationException ex) {
//                ex.printStackTrace(System.out);
//            } catch (SAXException ex) {
//                ex.printStackTrace(System.out);
//            } catch (IOException ex) {
//                ex.printStackTrace(System.out);
//            }
//        }
//    }
//
//    public Books deserializeXML(String pathXML) throws IOException {
//        File file = new File(pathXML);
//        XmlMapper xmlMapper = new XmlMapper();
//        String xml = inputStreamToString(new FileInputStream(file));
//        Books value = xmlMapper.readValue(xml, Books.class);
//        return value;
//    }
//
//    public Books deserializeJSON(String pathJSON) throws IOException {
//        File file = new File(pathJSON);
//        ObjectMapper jsonMapper = new ObjectMapper();
//        String json = inputStreamToString(new FileInputStream(file));
//        Books value = jsonMapper.readValue(json, Books.class);
//        return value;
//    }
//
//    public Map<String, Object> deserializeUnknownJSONFile() throws IOException {
//        String json = inputStreamToString(new FileInputStream("src\\main\\resources\\someJsonFile.json"));
//        ObjectMapper objectMapper = new ObjectMapper();
//        Map<String, Object> jsonMap = objectMapper.readValue(json, new TypeReference<Map<String,Object>>(){});
//        return jsonMap;
//    }
//
//
//
//    public String inputStreamToString(InputStream is) throws IOException {
//        StringBuilder sb = new StringBuilder();
//        String line;
//        BufferedReader br = new BufferedReader(new InputStreamReader(is));
//        while ((line = br.readLine()) != null) {
//            sb.append(line);
//        }
//        br.close();
//        return sb.toString();
//    }
//}
