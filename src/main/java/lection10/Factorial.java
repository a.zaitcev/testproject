public class Factorial{

    public static int getFactorial(int num) throws FactorialException{
        int result=1;
        if(num<1) throw new FactorialException("The number is less than 1", num);

        for(int i=1; i<=num;i++){

            result*=i;
        }
        return result;
    }

    public static void main(String[] args){

        try{
            int result = Factorial.getFactorial(-1);
            System.out.println(result);
        }
        catch(FactorialException ex){
            System.out.println(ex.getMessage());
            System.out.println(ex.getNumber());
            System.out.println(ex.printErrorCode());
        }
    }
}

class FactorialException extends Exception {

    private int number;
    private int errorCode;


    public FactorialException(String message, int num) {
        super(message);
        number=num;
        System.out.println("calling my custom exception class!");
        this.errorCode = 322;
    }

    public int printErrorCode(){
        return this.errorCode;
    }

    public int getNumber() {
        return this.number;
    }
}