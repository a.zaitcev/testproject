import java.io.*;
import java.util.Scanner;

public class TestException {
    public static void main(String[] args) {

//        TestException testException = new TestException();
//        testException.getSingleExceptionHandled();
//        testException.getSeveralExceptionHandled();


        // throws
//        try{
//            int result = getFactorial(-6);
//            System.out.println(result);
//        }
//        catch(Exception ex){
//            System.out.println(ex.getMessage());
//        }

//        int result = getFactorial(-6);
//        System.out.println(result);
//        TestException testException = new TestException();
//        testException.
        try{
            String filePath = "C:\\Users\\test_user\\IdeaProjects\\testproject\\src\\lection10\\test.txt";
            TestException.printFirstString(filePath);
        }
        catch (FileNotFoundException ex){
            System.out.println("File not found by path");
        }
        catch (IOException ex){
            System.out.println("IOException happened!");
        }




    }

    // обработанная ошибка в методе
    public void getSingleExceptionHandled(){
        try{
            int[] numbers = new int[3];
            numbers[4]=45;
            System.out.println(numbers[4]);
        }
        catch(Exception ex){
//        catch(ArrayIndexOutOfBoundsException ex){
//            ex.printStackTrace();
            System.out.println("This is a exception message: " + ex.getMessage());
        }
//        finally {
//            System.out.println("я все равно что-то сделаю!");
//        }
        System.out.println("Программа завершена");
    }

    public void getSeveralExceptionHandled() {
        int[] numbers = new int[3];
        try {
//            numbers[6]=45;
            numbers[6] = Integer.parseInt("gfd");
        }
//        catch (ArrayIndexOutOfBoundsException ex) {
//            System.out.println("Выход за пределы массива");
//        } catch (NumberFormatException ex) {
//            System.out.println("Ошибка преобразования из строки в число");
//        }
        catch(NumberFormatException | ArrayIndexOutOfBoundsException ex){
            System.out.println(ex.getMessage());
            System.out.println("something happened!");

        }
    }

    public static int getFactorial(int num) throws Exception {
        if(num<1) throw new Exception("The number is less than 1");
        int result=1;
        for(int i=1; i<=num;i++){

            result*=i;
        }
        return result;
    }

    public static void printFirstString(String filePath) throws FileNotFoundException, IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        String firstString = reader.readLine();
        System.out.println(firstString);
    }

    public void tryWithResourcesTest(){
//        Scanner scanner = null;
//        try {
//            scanner = new Scanner(new File("test.txt"));
//            while (scanner.hasNext()) {
//                System.out.println(scanner.nextLine());
//            }
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } finally {
//            if (scanner != null) {
//                scanner.close();
//            }
//        }

        try (Scanner scanner = new Scanner(new File("test.txt"))) {
            while (scanner.hasNext()) {
                System.out.println(scanner.nextLine());
            }
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        }
    }

}
