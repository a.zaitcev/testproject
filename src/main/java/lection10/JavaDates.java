import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class JavaDates {

    public static void main(String[] args) throws InterruptedException {

        String date = "31 июня 2018 года";
        System.out.println(date);

        Date dateNormal = new Date();
        System.out.println(dateNormal);
//
//        Thread.sleep(1000);
//        System.out.println(new Date());
//
//        Date date123 = new Date(1212121212121L);
//        System.out.println(date123);
//
//        JavaDates javaDates = new JavaDates();
//        javaDates.compareDates();
//
//        javaDates.dateBefore();
//        javaDates.dateAfter();
//        javaDates.dateEquals();

//        Date date = new Date();
//        int day = date.getHours();
//        System.out.println(day);


        // date format
        // Вот так выглядит пример создания объекта SimpleDateFormat и форматирования Date:
        // yyyy-MM-dd HH:mm:ss.SSS Z
        // dd-MM-yyyy
        // HH:mm:ss.SSS
//        SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
//        Date date333 = new Date();
//        System.out.println(formatter.format(date333));


        // make exception
//        String strDate = "Sat, April 4, 2020";
//        SimpleDateFormat formatter = new SimpleDateFormat("EEE, MMMM d, yyyy", Locale.ENGLISH);
//        try {
//            Date date1 = formatter.parse(strDate);
//            System.out.println(date1);
//            System.out.println(formatter.format(date1));
//        }
//        catch (ParseException e) {
//            e.printStackTrace();
//        }

//        SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE, d MMMM yyyy");
//        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
//
//        Calendar calendar = new GregorianCalendar(2007, Calendar.JANUARY , 25);
////        calendar.set(Calendar.HOUR, 10);
////        calendar.set(Calendar.MINUTE, 42);
////        calendar.set(Calendar.SECOND, 12);
//
//        calendar.roll(Calendar.MONTH, -2);
//        System.out.println("roll");
//        System.out.println(calendar.getTime().toString());
//
//
//        Calendar calendar1 = new GregorianCalendar(2007, Calendar.JANUARY , 25);
//        calendar1.add(Calendar.MONTH, -2);
//        System.out.println("add");
//        System.out.println(calendar1.getTime().toString());

//        System.out.println(dateFormat.format(calendar.getTime()));



    }

    public void compareDates() throws InterruptedException {
        Date date1 = new Date();

        Thread.sleep(1000);

        Date date2 = new Date();

        System.out.println((date1.getTime() > date2.getTime()) ? "date1 раньше date2" : "date2 позже date1");
    }

    public void dateBefore() throws InterruptedException {
        Date date1 = new Date();

        Thread.sleep(2000);//приостановим работу программы на 2 секунды
        Date date2 = new Date();

        System.out.println(date1.before(date2));
    }

    public void dateAfter() throws InterruptedException {
        Date date1 = new Date();

        Thread.sleep(2000);//приостановим работу программы на 2 секунды
        Date date2 = new Date();

        System.out.println(date1.after(date2));
    }

    public void dateEquals() throws InterruptedException {
        Date date1 = new Date();
        Thread.sleep(2);
        Date date2 = new Date();

        System.out.println(date1.getTime());
        System.out.println(date2.getTime());

        System.out.println(date1.equals(date2));
    }
}
