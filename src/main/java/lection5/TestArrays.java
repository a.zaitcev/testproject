package lection5;

import java.util.Arrays;
import java.util.Scanner;

public class TestArrays {
    public static void main(String[] args) {


//         1
        int[] myArray;

        myArray = new int[3];

        // нумерация индексов в массиве начинается с нуля
        // [11, 22, 33]
        //  0    1    2
        myArray[0] = 11;
        myArray[1] = 22;
        myArray[2] = 33;
        myArray[2] = (myArray[0] - myArray[1]) / 2;
        System.out.println(myArray[2]);
//
//        // вывод на экран
//        for (int i = 0; i < 3; i++) {
//            System.out.println(myArray[i]);
//        }
//
//        // изменение элемента
//        myArray[2] = 44;
//
//        // вывод отдельного элемента
//        System.out.println("i have printed element: " + myArray[2]);

//        System.out.println(myArray[33]);


        // 2
//        int[] myArray1 = new int[3];
//
//        // по умолчанию
//        System.out.println(myArray1[0]);
//        for(int i = 0; i < 3; i++){
//            System.out.println(myArray1[i]);
//        }
//        System.out.println(myArray1.length);

        // 3
//        String[] seasons  = new String[] {"Winter", "Spring", "Summer", "Autumn"};
//
//        for(int i = 0; i < 4; i++){
//            System.out.println(seasons[i]);
//        }
//
//        int[] myArray2 = {1,2,3};
//
//        // c-style
//        int myArray3[];


        // Arrays
//
//        int[] numbers = {167, -2, 16, 99, 26, 92, 43, -234, 35, 80};
//        int[] numbers2;
//        System.out.println("before");
//
//        // print
//        System.out.println(Arrays.toString(numbers));
////
//        Arrays.sort(numbers);
////
//        System.out.println("after sort");
//        System.out.println(Arrays.toString(numbers));
////
////        // copy
//        numbers2 = numbers;
//        numbers[0] = 33;
//        numbers[1] = 555;
//        System.out.println(Arrays.toString(numbers));
//        System.out.println(Arrays.toString(numbers2));
//
//
//
//        int [] numbersCopy = Arrays.copyOf(numbers, numbers.length);
//        System.out.println(Arrays.toString(numbersCopy));
//
//
//        //compare
//        int[] numbers11 = {1, 2, 3};
//        int[] numbers22 = {1, 2, 4};
//
//
//
//        // bad, сравнивает ссылки
//        System.out.println(numbers11.equals(numbers22));
//
//        System.out.println(Arrays.equals(numbers11, numbers22));

        // search
//        System.out.println(
//                99 + " found at index = "
//                        + Arrays.binarySearch(numbers, 99));
//        String[] seasons  = new String[] {"Winter", "Spring", "Summer", "Autumn"};
//        System.out.println(
//                "Summer1" + " found at index = "
//                        + Arrays.binarySearch(seasons, "Summer1"));



        // two dim array
//        int[][] myTwoDimentionalArray = new int [8][8];
//
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        for(int i =1; i <= n; i++){
            for(int j = 1; j <= m; j++){
                if (j == m){
                    System.out.println(i*j);
                }
                else{
                    System.out.print(i*j + " ");
                }
            }
        }

        // sample 2
        int[][] myArr = {{1, 2, 3}, {4}, {5, 6, 7, 8}};

        System.out.println(myArr[2][1]); // 7
//
//        myArr[2][2] = 42;
//
//        System.out.println(myArr[2][2]); // 42

    }
}
