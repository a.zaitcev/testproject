package lection5;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class JunctionSC {
    public static void main(String[] args) {
        // Определим текущий день недели
        DayOfWeek dayOfWeek = LocalDate.now().getDayOfWeek();

        int x = 3;

        switch (dayOfWeek) {
            case SUNDAY:
                System.out.println("Сегодня воскресенье");
                break;
            case MONDAY:
                System.out.println("Сегодня понедельник");
                break;
            case TUESDAY:
                System.out.println("Сегодня вторник");
                break;
            case WEDNESDAY:
                System.out.println("Сегодня среда");
//                break;
            case THURSDAY:
                System.out.println("Сегодня четверг");
                break;
            case FRIDAY:
                System.out.println("Сегодня пятница");
                break;
            case SATURDAY:
                System.out.println("Сегодня суббота");
                break;
        }


        // Определим текущий день недели
//        DayOfWeek dayOfWeek1 = LocalDate.now().getDayOfWeek();
//
//        switch (dayOfWeek1) {
//            case SUNDAY:
//            case SATURDAY:
//                System.out.println("Сегодня выходной");
//                break;
//            case FRIDAY:
//                System.out.println("Завтра выходной");
//                break;
//            default:
//                System.out.println("Сегодня рабочий день");
//                break;
//        }
    }
}

