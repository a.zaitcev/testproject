package lection5;

import java.util.Scanner;

public class forCycleTests {
    public static void main(String[] args) {
//        String[]  lst = {"Зима", "Лето", "Осень", "Весна"}; // 4
//        i++; // i = i + 1;
//        i--; // i = i - 1;
        // в прямом порядке
//        for (int i = 0; i < lst.length; i++) {
//            // i = 0
//            //
//            System.out.println(i);
//            System.out.println(lst[i] + " ");
//            // i = 1
//        }

        // в обратном порядке
//        for (int i = (lst.length - 1); i >= 0; i--) {
//            System.out.println(i);
//            System.out.println(lst[i] + " ");
//        }

//        // несколько условий
//        for(int a = 0, b = 0; a - b <= 10; a++, b--){
//            System.out.println(a);
//            System.out.println(b);
//            System.out.println(" " + a*b);
//            System.out.println('\n');
//        }
//
        // цикл while
//        String[] lst = {"Зима", "Лето", "Осень", "Весна"}; // 4
//        int i = 0;
//        while(i < lst.length){
//            System.out.print(lst[i] + " ");
//            i++;
//        }
//
//        int j = 0;
//        do {
//            j++;
//            System.out.println(Integer.valueOf(j) + " ");
//        } while(j < 5);

//        while(j <= 5){
//            System.out.println(Integer.valueOf(j) + " ");
//            j++;
//        }

//        String word;
//        Scanner sc = new Scanner(System.in);
//        do{
//            word = sc.nextLine();
//            System.out.println(word);
//        }
//        while(!word.equals("stop"));



        // Прерывание цикла for
//        for (int a = 1; a <= 10; a++){
//            System.out.println(a);
//            if (a == 5)
//                break;
//            System.out.println("a = " + a);
//        }

        // Прерывание цикла while
//        int s = 100;
//        while(true) {
//            System.out.println(s +" ");
//            s = s / 2;
//            if (s == 0)
//                break;
//            System.out.println("s = " + s);
//        }

//        int s = 1;
//        if(s == 0){ // true
//            System.out.println("s is equals zero");
//        }
//        else{
//            System.out.println("s is not equals zero");
//        }


        // Прерывание цикла while
//        for(int i = 0; i < 50; i+=2){
//            if(i == 10){
////                System.out.println("skip this digit");
//                break;
//            }
//            else{
//                System.out.println(i);
//            }
//        }
//        int s = 100;
//        while(true) {
//            System.out.print(s +" ");
//            s = s / 2;
//            if (s == 25)
//                continue;
//            else if (s == 0) {
//                break;
//            } else
//                System.out.println("s = " + s);
//        }
    }
}
